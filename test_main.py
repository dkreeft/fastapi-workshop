# You do not have to touch this file

import requests

import conf

BASE_URL = f"http://{conf.HOST}:{conf.PORT}/"


def test_get_endpoint():
    res = requests.get(url=BASE_URL)
    assert res.status_code == 200


def test_post_endpoint():
    res = requests.post(url=f"{BASE_URL}product", data={'product': 'test'})
    if res.status_code == 422:
        res = requests.post(url=f"{BASE_URL}product", params={'product': 'test'})
    assert res.status_code == 200
