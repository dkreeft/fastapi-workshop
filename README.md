# FastAPI workshop

This repository contains example files that can be used as a basis for completing the FastAPI workshop.


## Setup
Make sure you create a new virtual environment and install the requirements using:

```bash
python -m pip install -r requirements.txt
```

## Running an API
You can start the API anytime by running the following:

```bash
python main.py
```

## Testing your solutions
Run the following command to see if your API works correctly:

```bash
python -m pytest
```

**NOTE**: Make sure the API is running in a separate terminal window, otherwise the tests will fail.
