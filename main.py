import uvicorn
from fastapi import FastAPI


import conf

app = FastAPI()


# As a store owner I would like to see whether the API is still up and running by:
# sending me a response with its local time at the root ('/') endpoint



# As a store owner I would like to add a product to my store and have my request
# returned by:
# sending a request at '/product' with a query parameter 'product'



if __name__ == "__main__":
    uvicorn.run("main:app", host=conf.HOST, port=conf.PORT, reload=True, debug=True)
